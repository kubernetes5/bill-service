package com.ansp.microservices.bill;

import java.util.Date;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.env.Environment;

import com.ansp.microservices.bill.entities.Bill;
import com.ansp.microservices.bill.repository.BillRepository;



@SpringBootTest
class BillServiceApplicationTests {

	@Autowired
	private Environment env;
	
	@Autowired
	private BillRepository billService;
	
	@Test
	void contextLoads() {
		System.out.println(env.getProperty("product.url"));
	}
	
	@Test
	void BillCreateTest() {
		Bill bill = new Bill();
		bill.setCustomer(1);
		bill.setDate(new Date());
		bill.setTotal(100.00);
		bill.setId(1000);
		billService.save(bill);
	}
	
	@Test
	void BillUpdateTest() {
		Bill bill = new Bill();
		bill.setCustomer(1);
		bill.setDate(new Date());
		bill.setTotal(200.00);
		bill.setId(1000);
		billService.save(bill);
	}
	
	@Test
	void BillDeleteTest() {
		billService.deleteById(1000);
	}
}
