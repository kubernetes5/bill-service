package com.ansp.microservices.bill.entities;

import java.util.Date;

import javax.persistence.Id;

import org.springframework.beans.factory.annotation.Autowired;

public class Bill {
	@Id
	private int id;
	private int customer;
	private Double total;
	private Date date;
	private BillType billType;
	
	
	public Bill() {
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getCustomer() {
		return customer;
	}
	public void setCustomer(int customer) {
		this.customer = customer;
	}
	public Double getTotal() {
		return total;
	}
	public void setTotal(Double total) {
		this.total = total;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public BillType getBillType() {
		return billType;
	}
	public void setBillType(BillType billType) {
		this.billType = billType;
	}
	@Override
	public String toString() {
		return "id=" + id;
	}
	
	

}
