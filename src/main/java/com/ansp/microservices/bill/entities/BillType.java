package com.ansp.microservices.bill.entities;

import javax.persistence.Id;

public class BillType {
	@Id
	private int id;
	private String name;
	private String description;

	public BillType() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "id=" + id;
	}

}
