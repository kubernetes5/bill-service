package com.ansp.microservices.bill.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.ansp.microservices.bill.entities.Bill;
import com.ansp.microservices.bill.exception.BillNotFoundException;
import com.ansp.microservices.bill.repository.BillRepository;

@Service
public class BillService {
	@Autowired
	private BillRepository repository;

	@Autowired
	private Environment env;

	public List<Bill> findAll() {
		return (List<Bill>) this.repository.findAll();
	}

	public Bill findById(int id) throws Exception {
		return this.repository.findById(id).orElseThrow(() -> new BillNotFoundException("Bill no existente "));
	}

	public Bill save(Bill bill) {
		env.getProperty("customer.url","http://localhost:8080/customer");
		return this.repository.save(bill);

	}

	public Bill update(Bill bill) {

		return this.repository.save(bill);

	}

	public void deleteById(int id) {

		this.repository.deleteById(id);

	}

}
