package com.ansp.microservices.bill.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ansp.microservices.bill.entities.BillType;
import com.ansp.microservices.bill.exception.BillTypeNotFoundException;
import com.ansp.microservices.bill.repository.BillTypeRepository;

@Service
public class BillTypeService {

	@Autowired
	private BillTypeRepository repository;
	
	public List<BillType> findAll() {

		return (List<BillType>) this.repository.findAll();
	}

	public BillType findById(int id) throws Exception {

		return this.repository.findById(id).orElseThrow(() -> new BillTypeNotFoundException("BillType no existente "));
	}

	public BillType save(BillType billType) {

		return this.repository.save(billType);

	}

	public BillType update(BillType billType) {

		return this.repository.save(billType);

	}

	public void deleteById(int id) {

		this.repository.deleteById(id);

	}
}
