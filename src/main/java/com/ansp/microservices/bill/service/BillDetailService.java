package com.ansp.microservices.bill.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.ansp.microservices.bill.entities.BillDetail;
import com.ansp.microservices.bill.exception.BillDetailNotFoundException;
import com.ansp.microservices.bill.repository.BillDetailRepository;

@Service
public class BillDetailService {

	@Autowired
	private BillDetailRepository repository;
	
	@Autowired
	private Environment env;
	
	public List<BillDetail> findAll() {

		return (List<BillDetail>) this.repository.findAll();
	}

	public BillDetail findById(int id) throws Exception {

		return this.repository.findById(id).orElseThrow(() -> new BillDetailNotFoundException("BillDetail no existente "));
	}

	public BillDetail save(BillDetail billDetail) {
		env.getProperty("product.url","http://localhost:8080/product");
		return this.repository.save(billDetail);

	}

	public BillDetail update(BillDetail billDetail) {

		return this.repository.save(billDetail);

	}

	public void deleteById(int id) {

		this.repository.deleteById(id);

	}
}
