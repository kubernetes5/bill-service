package com.ansp.microservices.bill.exception;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND) 
public class BillTypeNotFoundException extends RuntimeException{
	public BillTypeNotFoundException(String message) {
		super(message);
	}
}