package com.ansp.microservices.bill.repository;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.ansp.microservices.bill.entities.BillType;

//@Repository
public interface BillTypeRepository  extends MongoRepository<BillType, Integer>{

}
