package com.ansp.microservices.bill.repository;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.ansp.microservices.bill.entities.Bill;

//@Repository
public interface BillRepository  extends MongoRepository<Bill, Integer>{

}
