package com.ansp.microservices.bill.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.ansp.microservices.bill.entities.BillDetail;
import com.ansp.microservices.bill.service.BillDetailService;

import io.swagger.annotations.ApiOperation;

@RestController
public class BillDetailController {
	@Autowired
	private BillDetailService service;

	@ApiOperation("Retorna los bill details")
	@GetMapping("/billDetails")
	public List<BillDetail> findAll() {
		return this.service.findAll();
	}
	
	@ApiOperation("Inserta un bill detail")
	@PostMapping("/billDetails")
	public ResponseEntity<BillDetail> save(@Valid @RequestBody BillDetail billDetail) {
		return new ResponseEntity<BillDetail>(service.save(billDetail),HttpStatus.CREATED);
	}
	
	@ApiOperation("Actualiza un bill detail")
	@PutMapping("/billDetails/{id}")
	public BillDetail update(@RequestBody BillDetail billDetail, @PathVariable("id") Integer id) {
		billDetail.setId(id);
		return this.service.update(billDetail);
	}
	@ApiOperation("Retorna un bill detail")
	@GetMapping("/billDetails/{id}")
	public BillDetail findById(@PathVariable("id") Integer id) throws Exception {
		return this.service.findById(id);
	}
	@ApiOperation("Borra un bill detail")
	@DeleteMapping("/billDetails/{id}")
	public void delete(@PathVariable("id") Integer id) {
		this.service.deleteById(id);
	}
}
