package com.ansp.microservices.bill.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.ansp.microservices.bill.entities.BillType;
import com.ansp.microservices.bill.service.BillTypeService;

import io.swagger.annotations.ApiOperation;

@RestController
public class BillTypeController {
	@Autowired
	private BillTypeService service;

	@ApiOperation("Muestra todos los bill types")
	@GetMapping("/billTypes")
	public List<BillType> findAll() {
		return this.service.findAll();
	}
	
	@ApiOperation("Inserta un bill type")
	@PostMapping("/billTypes")
	public ResponseEntity<BillType> save(@Valid @RequestBody BillType billType) {
		return new ResponseEntity<BillType>(service.save(billType),HttpStatus.CREATED);
	}
	
	@ApiOperation("Actualiza un bill type")
	@PutMapping("/billTypes/{id}")
	public BillType update(@RequestBody BillType billType, @PathVariable("id") Integer id) {
		billType.setId(id);
		return this.service.update(billType);
	}
	@ApiOperation("Retorna un bill type")
	@GetMapping("/billTypes/{id}")
	public BillType findById(@PathVariable("id") Integer id) throws Exception {
		return this.service.findById(id);
	}
	@ApiOperation("Borra un bill type")
	@DeleteMapping("/billTypes/{id}")
	public void delete(@PathVariable("id") Integer id) {
		this.service.deleteById(id);
	}
}
