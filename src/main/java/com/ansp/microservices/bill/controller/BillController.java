package com.ansp.microservices.bill.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.ansp.microservices.bill.entities.Bill;
import com.ansp.microservices.bill.service.BillService;

import io.swagger.annotations.ApiOperation;

@RestController
public class BillController {
	@Autowired
	private BillService service;

	@ApiOperation("Retorna los bills")
	@GetMapping("/bills")
	public List<Bill> findAll() {
		return this.service.findAll();
	}
	
	@ApiOperation("Inserta un bill")
	@PostMapping("/bills")
	public ResponseEntity<Bill> save(@Valid @RequestBody Bill bill) {
		return new ResponseEntity<Bill>(service.save(bill),HttpStatus.CREATED);
	}
	
	@ApiOperation("Actualiza un bill")
	@PutMapping("/bills/{id}")
	public Bill update(@RequestBody Bill bill, @PathVariable("id") Integer id) {
		bill.setId(id);
		return this.service.update(bill);
	}
	@ApiOperation("Retorna un bill")
	@GetMapping("/bills/{id}")
	public Bill findById(@PathVariable("id") Integer id) throws Exception {
		return this.service.findById(id);
	}
	@ApiOperation("Borra un bill")
	@DeleteMapping("/bills/{id}")
	public void delete(@PathVariable("id") Integer id) {
		this.service.deleteById(id);
	}
}
